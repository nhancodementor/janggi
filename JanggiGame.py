from enum import Enum

class GameState(Enum):
    UNFINISHED = 'UNFINISHED'
    RED_WON = 'RED_WON'
    BLUE_WON = 'BLUE_WON'

class GameSide(Enum):
    RED = 'red'
    BLUE = 'blue'

class Piece:
    """The class Piece is to handle a piece in the game.
    
    Attributes:
        _name (str): Name of the piece
        _game (JanggiGame): The game that the piece belongs to
        _pos ([]): Position of the piece
        _side (enum GameSide): Side name of the piece
    """

    def __init__(self, name, game, pos, side):
        """The __init__ method.
    
        Args:
            name (str): Name of the piece
            game (JanggiGame): The game that the piece belongs to
            pos ([]): Position of the piece
            side (enum GameSide): Side name of the piece
        """

        self._name = name # such as 'General', 'Guard', 'Horse', 'Elephant', 'Chariot', 'Cannon', 'Soldier'
        self._game = game
        self._pos = pos
        self._side = side

    def __str__(self):
        """str: A string to know the name and the position of the piece."""
        return self._name + " at " + chr(ord('a') + self._pos[0]) + str(1 + self._pos[1])

    def short_name(self):
        """str: A short name of the piece."""
        return self._name[0:2] + self._side.value[0].upper()

    def has_valid_move(self, pos, destination_piece_name):
        """The has_valid_move method is to check if the piece can move to a position

        Args:
            pos ([]): The destination position
            destination_piece_name (str, optional): The piece name at the destination position if exists
        """

        # trivial test: Cannot move pieces out of the board
        if pos[0] < 0 or pos[0] > 8 or pos[1] < 0 or pos[1] > 9:
            return False

        # generals and guards aren't allowed to leave the palace
        if self._name == 'General' or self._name == 'Guard':
            if pos[0] < 3 or pos[0] > 5 or (self._side == GameSide.BLUE and pos[1] < 7) or (self._side == GameSide.RED and pos[1] > 2):
                return False
            # move one step per turn along marked lines in the palace
            if (abs(self._pos[0] - pos[0]) + abs(self._pos[1] - pos[1]) == 1 or self.move_in_palace(pos)):
                return True
            return False

        # horses and elephants can be blocked
        elif self._name == 'Horse':
            if ((self._pos[0] - pos[0] == 1 and self._pos[1] - pos[1] == 2 and self._pos[1] > 0 and self._game._board[self._pos[1] - 1][self._pos[0]] == 0)
                or (self._pos[0] - pos[0] == -1 and self._pos[1] - pos[1] == 2 and self._pos[1] > 0 and self._game._board[self._pos[1] - 1][self._pos[0]] == 0)
                or (self._pos[0] - pos[0] == 1 and self._pos[1] - pos[1] == -2 and self._pos[1] < 9 and self._game._board[self._pos[1] + 1][self._pos[0]] == 0)
                or (self._pos[0] - pos[0] == -1 and self._pos[1] - pos[1] == -2 and self._pos[1] < 9 and self._game._board[self._pos[1] + 1][self._pos[0]] == 0)
                or (self._pos[0] - pos[0] == 2 and self._pos[1] - pos[1] == 1 and self._pos[0] > 0 and self._game._board[self._pos[1]][self._pos[0] - 1] == 0)
                or (self._pos[0] - pos[0] == 2 and self._pos[1] - pos[1] == -1 and self._pos[0] > 0 and self._game._board[self._pos[1]][self._pos[0] - 1] == 0)
                or (self._pos[0] - pos[0] == -2 and self._pos[1] - pos[1] == 1 and self._pos[0] < 8 and self._game._board[self._pos[1]][self._pos[0] + 1] == 0)
                or (self._pos[0] - pos[0] == -2 and self._pos[1] - pos[1] == -1 and self._pos[0] < 8 and self._game._board[self._pos[1]][self._pos[0] + 1] == 0)
                ):
                return True
            return False

        elif self._name == 'Elephant':
            if ((self._pos[0] - pos[0] == 2 and self._pos[1] - pos[1] == 3 and self._pos[1] > 0 and self._game._board[self._pos[1] - 1][self._pos[0]] == 0)
                or (self._pos[0] - pos[0] == -2 and self._pos[1] - pos[1] == 3 and self._pos[1] > 0 and self._game._board[self._pos[1] - 1][self._pos[0]] == 0)
                or (self._pos[0] - pos[0] == 2 and self._pos[1] - pos[1] == -3 and self._pos[1] < 9 and self._game._board[self._pos[1] + 1][self._pos[0]] == 0)
                or (self._pos[0] - pos[0] == -2 and self._pos[1] - pos[1] == -3 and self._pos[1] < 9 and self._game._board[self._pos[1] + 1][self._pos[0]] == 0)
                or (self._pos[0] - pos[0] == 3 and self._pos[1] - pos[1] == 2 and self._pos[0] > 0 and self._game._board[self._pos[1]][self._pos[0] - 1] == 0)
                or (self._pos[0] - pos[0] == 3 and self._pos[1] - pos[1] == -2 and self._pos[0] > 0 and self._game._board[self._pos[1]][self._pos[0] - 1] == 0)
                or (self._pos[0] - pos[0] == -3 and self._pos[1] - pos[1] == 2 and self._pos[0] < 8 and self._game._board[self._pos[1]][self._pos[0] + 1] == 0)
                or (self._pos[0] - pos[0] == -3 and self._pos[1] - pos[1] == -2 and self._pos[0] < 8 and self._game._board[self._pos[1]][self._pos[0] + 1] == 0)
                ):
                return True
            return False

        elif self._name == 'Chariot':
            if ((self.get_nb_interposed_pieces(pos, 0) == 0 and (self._pos[0] == pos[0] or self._pos[1] == pos[1])) 
                or self.move_in_palace(pos)
                ):
                return True
            return False

        # cannons cannot capture other cannons
        elif self._name == 'Cannon':
            if destination_piece_name == 'Cannon':
                return False
            if self.get_nb_interposed_pieces(pos, 0) == 1 and self.get_nb_interposed_pieces(pos, 'Cannon') == 0:
                return True
            return False

        elif self._name == 'Soldier':
            if ((abs(self._pos[0] - pos[0]) + abs(self._pos[1] - pos[1]) == 1 or self.move_in_palace(pos))
                # soldier can only move forward or sideway
                and ((self._side == GameSide.BLUE and self._pos[1] >= pos[1])
                    or (self._side == GameSide.RED and self._pos[1] <= pos[1])
                    )
                ):
                return True
            return False
        return False
    
    def get_nb_interposed_pieces(self, pos, piece_name):
        """The get_nb_interposed_pieces method is to compute number of pieces between the piece and the destination position

        Args:
            pos ([]): The destination position
            piece_name (str, optional): Used to compute number of pieces with the specific piece_name between the piece and the destination position
        """
        if self._pos[0] == pos[0]:
            nb_pieces = 0
            for i in range(self._pos[1], pos[1], 1 if self._pos[1] < pos[1] else -1):
                if self._game._board[i][pos[0]] != 0 and (piece_name == 0 or self._game._board[i][pos[0]]._name == piece_name):
                    nb_pieces = nb_pieces + 1
            return nb_pieces - 1

        if self._pos[1] == pos[1]:
            nb_pieces = 0
            for i in range(self._pos[0], pos[0], 1 if self._pos[0] < pos[0] else -1):
                if self._game._board[pos[1]][i] != 0 and (piece_name == 0 or self._game._board[i][pos[0]]._name == piece_name):
                    nb_pieces = nb_pieces + 1
            return nb_pieces - 1
        
        return 0

    def move_in_palace(self, pos):
        """The move_in_palace method is to check if the piece can move in the palace

        Args:
            pos ([]): The destination position
        """
        return ((self._pos == [3, 0] and pos == [4, 1])
                or (self._pos == [5, 0] and pos == [4, 1])
                or (self._pos == [3, 2] and pos == [4, 1])
                or (self._pos == [5, 2] and pos == [4, 1])
                or (self._pos == [4, 1] and (pos == [3, 0] or pos == [5, 0] or pos == [3, 2] or pos == [5, 2]))
                or (self._pos == [3, 7] and pos == [4, 8])
                or (self._pos == [5, 7] and pos == [4, 8])
                or (self._pos == [3, 9] and pos == [4, 8])
                or (self._pos == [5, 9] and pos == [4, 8])
                or (self._pos == [4, 8] and (pos == [3, 7] or pos == [5, 7] or pos == [3, 9] or pos == [5, 9]))
                )

INITIALPOSITION = {
    GameSide.RED: {
        'General': [[4, 1]],
        'Guard': [[3, 0], [5, 0]],
        'Horse': [[2, 0], [7, 0]],
        'Elephant': [[1, 0], [6, 0]],
        'Chariot': [[0, 0], [8, 0]],
        'Cannon': [[1, 2], [7, 2]],
        'Soldier': [[0, 3], [2, 3], [4, 3], [6, 3], [8, 3]]
    },
    GameSide.BLUE: {
        'General': [[4, 8]],
        'Guard': [[3, 9], [5, 9]],
        'Horse': [[2, 9], [7, 9]],
        'Elephant': [[1, 9], [6, 9]],
        'Chariot': [[0, 9], [8, 9]],
        'Cannon': [[1, 7], [7, 7]],
        'Soldier': [[0, 6], [2, 6], [4, 6], [6, 6], [8, 6]]
    }
}

class JanggiSide:
    """The class JanggiSide is to handle a side in the game (either blue or red).
    
    Attributes:
        _name (enum GameSide): Name of the side
        _game (JanggiGame): The game that the side belongs to
        _pieces ([obj]): Store all live pieces of the side in the game
    """

    def __init__(self, game, name):
        """The __init__ method.
    
        Args:
            name (str): Name of the side
            game (JanggiGame): The game that the side belongs to
        """

        self._name = name
        self._game = game
        self._pieces = []
        self.init_pieces()
    
    def __str__(self):
        """str: A string to know the side name and all the pieces."""
        s = "Side " + self._name.value + ": "
        for piece in self._pieces:
            s += str(piece)
            s += ", "
        return s

    def init_pieces(self):
        """The init_pieces is to put all pieces in initial positions."""
        for piece_name in ('General', 'Guard', 'Horse', 'Elephant', 'Chariot', 'Cannon', 'Soldier'):
            nb_pieces = 0
            if piece_name == 'General':
                nb_pieces = 1
            elif piece_name == 'Guard' or piece_name == 'Horse' or piece_name == 'Elephant' or piece_name == 'Chariot' or piece_name == 'Cannon':
                nb_pieces = 2
            elif piece_name == 'Soldier': 
                nb_pieces = 5

            for i in range(nb_pieces):
                pos = INITIALPOSITION[self._name][piece_name][i]
                piece = Piece(piece_name, self._game, pos, self._name)
                self._game._board[pos[1]][pos[0]] = piece
                self._pieces.append(piece)
    
    def remove_piece(self, piece):
        """The remove_piece is to remove a piece out of the piece list when it is captured by the opponent.
        
        Args:
            piece (Piece): The captured piece
        """
        self._pieces.remove(piece)

class JanggiGame:
    """Main class of the game

    Attributes:
        _board ([][]): 2d array to store pieces in the game
        _blue_side (JanggiSide): blue side of the game
        _red_side (JanggiSide): red side of the game
        _turn (enum GameSide): to know who plays at the next turn
    """
    def __init__(self):
        """The __init__ method."""

        self._board = [[0 for i in range(9)] for j in range(10)] # 2d array having 10 rows, 9 columns
        self._blue_side = JanggiSide(self, GameSide.BLUE);
        self._red_side = JanggiSide(self, GameSide.RED);
        self._turn = GameSide.BLUE

    def get_game_state(self):
        """The get_game_state methode is to return the game state: 'UNFINISHED' or 'RED_WON' or 'BLUE_WON'."""
        if self._turn == GameSide.BLUE:
            if self.is_in_check(GameSide.RED.value):
                return GameState.BLUE_WON
            else:
                return GameState.UNFINISHED
        elif self._turn == GameSide.RED:
            if self.is_in_check(GameSide.BLUE.value):
                return GameState.RED_WON
            else:
                return GameState.UNFINISHED
        else:
            print("Should not happen!")
            return GameState.UNFINISHED

    def is_in_check(self, side):
        """The is_in_check methode is to test if a side is in check.
        
        Args:
            side (str): The side to be tested
        """

        # A general is in check if it could be captured on the opposing player's turn.
        if side == GameSide.BLUE.value:
            # find position of the Blue general
            blue_general = None
            for piece in self._blue_side._pieces:
                if piece._name == 'General':
                    blue_general = piece
                    break

            # check if one red piece can capture the blue general    
            for piece in self._red_side._pieces:
                if piece.has_valid_move(blue_general._pos, 'General'):
                    return True
            return False
        elif side == GameSide.RED.value:
            # find position of the Red general
            red_general = None
            for piece in self._red_side._pieces:
                if piece._name == 'General':
                    red_general = piece
                    break

            # check if one blue piece can capture the red general
            for piece in self._blue_side._pieces:
                if piece.has_valid_move(red_general._pos, 'General'):
                    return True
            return False
        else:
            print("Invalid parameter!")
            return False
    
    def make_move(self, origin, destination):
        """The make_move methode is to move a piece from origin to destination.
        
        Args:
            origin (str): The origin position
            destination (Str) : The destination position
        """

        #print("Move from " + origin + " to " + destination)
        x1 = ord(origin[0]) - ord('a')
        y1 = int(origin[1:]) - 1
        #print("x1 = " + str(x1) + " y1 = " + str(y1))
        piece = self._board[y1][x1]

        if piece != 0:
            if piece._side != self._turn:
                #print("It's not " + piece._side.value + "'s turn!")
                return False

            if self.get_game_state() == GameState.BLUE_WON or self.get_game_state() == GameState.RED_WON:
                print("Game is finished! " + self.get_game_state().value)
                return False
            
            # It is the turn of the piece's side
            if origin == destination: # pass turn
                # Switch turn 
                if self._turn == GameSide.BLUE:
                    self._turn = GameSide.RED
                else:
                    self._turn = GameSide.BLUE
                return True
            
            x2 = ord(destination[0]) - ord('a')
            y2 = int(destination[1:]) - 1
            #print("x2 = " + str(x2) + " y2 = " + str(y2))
            piece_in_destination = self._board[y2][x2]
            if (piece_in_destination != 0 and piece._side == piece_in_destination._side):
                print("Destination occupied by a piece of same side!")
                return False

            # Valid move
            if piece.has_valid_move([x2, y2], piece_in_destination._name if piece_in_destination != 0 else 0):
                self._board[y1][x1] = 0
                piece._pos = [x2, y2]
                self._board[y2][x2] = piece

                # Kill opponent's piece if any and Switch turn 
                if self._turn == GameSide.BLUE:
                    if piece_in_destination != 0:
                        self._red_side.remove_piece(piece_in_destination)
                    self._turn = GameSide.RED
                else:
                    if piece_in_destination != 0:
                        self._blue_side.remove_piece(piece_in_destination)
                    self._turn = GameSide.BLUE
                #self.print_board()
                return True
            else:
                return False

        print("No piece in the origin!")
        return False

    def print_board(self):
        """str: A string to present the game at an instant."""
        for i in range(len(self._board)):
            for j in range(len(self._board[i])):
                print(self._board[i][j].short_name() if self._board[i][j] != 0  else ' - ', end = ' ')
            print()
        print()

if __name__ == '__main__':
    game = JanggiGame()
    
    move_result = game.make_move('c1', 'e3') # should be False because it's not Red's turn
    move_result = game.make_move('a7', 'b7') # should return True
    blue_in_check = game.is_in_check('blue') # should return False
    move_result = game.make_move('a4', 'a5') # should return True
    state = game.get_game_state() # should return UNFINISHED
    move_result = game.make_move('b7','b6') # should return True
    move_result = game.make_move('b3','b6') # should return False because it's an invalid move
    move_result = game.make_move('a1','a4') # should return True
    move_result = game.make_move('c7','d7') # should return True
    move_result = game.make_move('a4','a4') # this will pass the Red's turn and return True
    move_result = game.make_move('c10','d8') # move blue horse and return True